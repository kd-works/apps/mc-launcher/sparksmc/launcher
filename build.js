/* eslint-disable indent */
const { build, Platform } = require('electron-builder')

function getCurrentPlatform() {
  switch (process.platform) {
    case 'win32':
      return Platform.WINDOWS
    case 'darwin':
      return Platform.MAC
    case 'linux':
      return Platform.LINUX
    default:
      console.error('Cannot resolve current platform!')
      return undefined
  }
}

build({
  targets: (process.argv[2] != null && Platform[process.argv[2]] != null ? Platform[process.argv[2]] : getCurrentPlatform()).createTarget(),
  config: {
    appId: 'fr.sparksmc.launcher',
    productName: 'Launcher SparksMC',
    copyright: 'Copyright © 2019 Kasai.',
    artifactName: '${name}-${version}-${arch}.${ext}',
    generateUpdatesFilesForAllChannels: true,
    remoteBuild: true,
    removePackageScripts: true,
    directories: {
      buildResources: 'build',
      output: 'releases/${os}'
    },
    publish: [{
      provider: 'generic',
      url: 'https://api.sparksmc.fr/launcher/releases/${os}'
    }],
    mac: {
      target: 'dmg',
      category: 'public.app-category.games',
      icon: 'build/icons/icon.icns',
      darkModeSupport: true
    },
    dmg: {
      // background: '',
      icon: 'build/icons/icon.icns',
      internetEnabled: true
    },
    win: {
      target: [{
        target: 'nsis',
        arch: 'x64'
      }, {
        target: 'nsis',
        arch: 'ia32'
      }],
      icon: 'build/icons/icon.ico',
      legalTrademarks: 'Copyright © 2019 Kasai.',
      publisherName: 'Kasai.'
    },
    nsis: {
      oneClick: false,
      perMachine: true,
      allowElevation: true,
      allowToChangeInstallationDirectory: true,
      installerIcon: 'build/icons/icon.ico',
      uninstallerIcon: 'build/icons/icon.ico',
      // installerHeader: '',
      // installerHeaderIcon: '',
      // installerSidebar: '',
      // uninstallerSidebar: '',
      deleteAppDataOnUninstall: true
    },
    linux: {
      target: [{
        target: 'AppImage',
        arch: 'x64'
      }, {
        target: 'AppImage',
        arch: 'ia32'
      }],
      icon: 'build/icons/1024x1024.png',
      synopsis: 'Launcher Minecraft Custom',
      category: 'Game'
    },
    compression: 'maximum',
    files: [
      '!{.vscode,releases,.editorconfig,.eslintrc.js,.gitignore,.gitlab-ci.yml,build.js}'
    ],
    extraResources: [
      'libraries'
    ],
    asar: true
  }
}).then(() => {
  console.log('Build complete!')
}).catch(err => {
  console.error('Error during build!', err)
})
