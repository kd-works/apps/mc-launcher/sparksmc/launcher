module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  env: {
    es6: true,
    node: true
  },
  extends: [
    'standard',
    'eslint:recommended'
  ],
  rules: {
    indent: [
      'error',
      2
    ],
    quotes: [
      'error',
      'single'
    ],
    semi: [
      'error',
      'never'
    ],
    'space-before-function-paren': [
      'error',
      'never'
    ],
    'no-var': 'error',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-control-regex': 'error',
    'no-unused-vars': [
      'error',
      {
        'vars': 'all',
        'args': 'none',
        'ignoreRestSiblings': false,
        'argsIgnorePattern': 'reject'
      }
    ],
    'no-async-promise-executor': [0]
  },
  overrides: [{
    files: [
      'app/assets/js/scripts/*.js'
    ],
    rules: {
      'no-unused-vars': [0],
      'no-undef': [0]
    }
  }, {
    files: [
      'build.js'
    ],
    rules: {
      'no-template-curly-in-string': 'off',
    }
  }]
}
