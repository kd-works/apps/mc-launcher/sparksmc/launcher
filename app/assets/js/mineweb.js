/* eslint-disable prefer-promise-reject-errors */
/**
 * Mojang
 *
 * This module serves as a minimal wrapper for Mojang's REST api.
 *
 * @module mojang
 */
// Requirements
const request = require('request')
const { createHash } = require('crypto')
const uuidv4 = require('uuid/v4')
const logger = require('./loggerutil')('%c[Mojang]', 'color: #a02d2a; font-weight: bold')
const { captureException } = require('./sentry')

// Constants
const authpath = 'https://sparksmc.fr/API/launcher'

const statuses = [{
  service: 'sparksmc.fr',
  status: 'grey',
  name: 'SparksMC.fr',
  essential: true
}, {
  service: 'api.sparksmc.fr',
  status: 'grey',
  name: 'API',
  essential: false
}, {
  service: 'play.sparksmc.fr',
  status: 'grey',
  name: 'Serveur Minecraft',
  essential: true
}]

const answerFromMojang = {
  accessToken: 'random access token', // hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key 'yggt']
  clientToken: 'client identifier', // identical to the one received
  availableProfiles: [{ // only present if the agent field was received
    agent: 'minecraft', // Presumably same value as before
    id: 'profile identifier', // hexadecimal
    name: 'player name',
    userId: 'hex string',
    createdAt: 1325376000000, // Milliseconds since Jan 1 1970
    legacyProfile: false, // Present even when false
    suspended: false, // probably false
    paid: true, // probably true
    migrated: true, // Seems to be false even for migrated accounts...?  (https://bugs.mojang.com/browse/WEB-1461)
    legacy: false // Only appears in the response if true. Default to false.  Redundant to the newer legacyProfile...
  }],
  selectedProfile: { // only present if the agent field was received
    id: '%uuid%',
    name: '%name%',
    userId: '%userId%',
    createdAt: 1325376000000,
    legacyProfile: false,
    suspended: false,
    paid: true,
    migrated: true,
    legacy: false
  },
  user: { // only present if requestUser was true in the request payload
    id: 'user identifier', // hexadecimal
    email: 'user@email.example', // Hashed(?) value for unmigrated accounts
    username: 'user@email.example', // Regular name for unmigrated accounts, email for migrated ones
    registerIp: '198.51.100.*', // IP address with the last digit censored
    migratedFrom: 'minecraft.net',
    migratedAt: 1420070400000,
    registeredAt: 1325376000000, // May be a few minutes earlier than createdAt for profile
    passwordChangedAt: 1569888000000,
    dateOfBirth: -2208988800000,
    suspended: false,
    blocked: false,
    secured: true,
    migrated: false, // Seems to be false even when migratedAt and migratedFrom are present...
    emailVerified: true,
    legacyUser: false,
    verifiedByParent: false,
    properties: [{
      name: 'preferredLanguage', // might not be present for all accounts
      value: 'en' // Java locale format (https://docs.oracle.com/javase/8/docs/api/java/util/Locale.html#toString--)
    }, {
      name: 'twitch_access_token', // only present if a twitch account is associated (see https://account.mojang.com/me/settings)
      value: 'twitch oauth token' // OAuth 2.0 Token; alphanumerical; e.g. https://api.twitch.tv/kraken?oauth_token=[...]
      // the Twitch API is documented here: https://github.com/justintv/Twitch-API
    }]
  }
}

// Functions

/**
 * Converts a Mojang status color to a hex value. Valid statuses
 * are 'green', 'yellow', 'red', and 'grey'. Grey is a custom status
 * to our project which represents an unknown status.
 *
 * @param {string} status A valid status code.
 * @returns {string} The hex color of the status code.
 */
exports.statusToHex = function(status) {
  switch (status.toLowerCase()) {
  case 'green':
    return '#a5c325'
  case 'yellow':
    return '#eac918'
  case 'red':
    return '#c32625'
  case 'grey':
  default:
    return '#848484'
  }
}

/**
 * Retrieves the status of Mojang's services.
 * The response is condensed into a single object. Each service is
 * a key, where the value is an object containing a status and name
 * property.
 *
 * @see http://wiki.vg/Mojang_API#API_Status
 */
exports.status = function() {
  return new Promise((resolve, reject) => {
    for (let i = 0; i < statuses.length; i++) {
      statuses[i].status = 'green'

      // Crashing API, we can't use this
      // Not a trustworthy source, but work as intended, stick to this third party for now
      // rp('https://steakovercooked.com/api/ping/?host=' + statuses[i].service, {
      //   json: true,
      //   timeout: 2500
      // })
      //   .then(function(htmlString) {
      //     statuses[i].status = 'green'
      //   })
      //   .catch(function(err) {
      //     captureException(err)

      //     logger.warn('Unable to retrieve Mojang status.')
      //     logger.debug('Error while retrieving Mojang statuses:', err)
      //     // reject(error || response.statusCode)
      //     statuses[i].status = 'grey'
      //   })
    }

    setTimeout(() => {
      // Give the time to the async request to be processed
      resolve(statuses)
    }, 5)
  })
}

/**
 * Authenticate a user with their Mojang credentials.
 *
 * @param {string} username The user's username, this is often an email.
 * @param {string} password The user's password.
 * @param {string} clientToken The launcher's Client Token.
 * @param {boolean} requestUser Optional. Adds user object to the reponse.
 * @param {Object} agent Optional. Provided by default. Adds user info to the response.
 *
 * @see http://wiki.vg/Authentication#Authenticate
 */
exports.authenticate = function(username, password) {
  return new Promise((resolve, reject) => {
    const AuthPassword = createHash('sha256').update(password).digest('hex')

    answerFromMojang.selectedProfile.id = uuidv4()
    answerFromMojang.selectedProfile.name = username

    request.get(authpath + '/' + username + '/' + AuthPassword, { json: true }, (error, response, body) => {
      if (error) {
        captureException(error)

        logger.error('Error during authentication.', error)
        reject(error)
      } else {
        if (response.statusCode === 200) {
          if (body.status === true) resolve(answerFromMojang)
          else reject({ code: 'ENOTFOUND' })
        } else reject({ code: 'ENOTFOUND' })
      }
    })
  })
}
