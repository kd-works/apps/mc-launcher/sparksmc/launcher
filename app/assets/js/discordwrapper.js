// Work in progress
const { Client } = require('discord-rpc')
const logger = require('./loggerutil')('%c[DiscordWrapper]', 'color: #7289da; font-weight: bold')

let client
let activity

exports.initRPC = function(genSettings, servSettings, initialDetails = 'En attente du Client..') {
  client = new Client({ transport: 'ipc' })

  activity = {
    details: initialDetails,
    state: 'Serveur : ' + servSettings.shortId,
    largeImageKey: servSettings.largeImageKey,
    largeImageText: servSettings.largeImageText,
    smallImageKey: genSettings.smallImageKey,
    smallImageText: genSettings.smallImageText,
    startTimestamp: new Date().getTime(),
    instance: false
  }

  client.on('ready', () => {
    logger.log('Discord RPC Connected')

    client.setActivity(activity)
      .catch(err => {
        logger.log(err)
      })
  })

  client.login({ clientId: genSettings.clientId })
    .catch(error => {
      if (error.message.includes('ENOENT')) logger.log('Unable to initialize Discord Rich Presence, no client detected.')
      else logger.log('Unable to initialize Discord Rich Presence: ' + error.message, error)
    })
}

exports.updateDetails = details => {
  activity.details = details
  client.setActivity(activity)
    .catch(err => {
      logger.log(err)
    })
}

exports.shutdownRPC = () => {
  if (!client) return

  client.clearActivity()
    .catch(err => {
      logger.log(err)
    })

  client.destroy()
    .catch(err => {
      logger.log(err)
    })

  client = null
  activity = null
}
