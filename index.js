// Requirements
const { app, BrowserWindow, ipcMain, Menu } = require('electron')
const { autoUpdater } = require('electron-updater')
const { join } = require('path')
const { format } = require('url')
const { prerelease } = require('semver')

const isDev = require('./app/assets/js/isdev')
const { init, captureException } = require('./app/assets/js/sentry')

require('ejs-electron')

init({
  dsn: 'https://a618c0dc979948e49d2971d163d016d3@sentry.io/1817583',
  release: require('./package.json').version,
  attachStacktrace: true,
  frameContextLines: 99999999999,
  maxBreadcrumbs: 100,
  maxValueLength: 99999999999
})

// Setup auto updater.
function initAutoUpdater(event, data) {
  if (data) autoUpdater.allowPrerelease = true
  else {
    // Defaults to true if application version contains prerelease components (e.g. 0.12.1-alpha.1)
    // autoUpdater.allowPrerelease = true
  }

  if (isDev) {
    autoUpdater.autoInstallOnAppQuit = true
    // autoUpdater.updateConfigPath = path.join(__dirname, 'dev-app-update.yml')
  }

  if (process.platform === 'darwin') autoUpdater.autoDownload = false

  autoUpdater.on('update-available', info => event.sender.send('autoUpdateNotification', 'update-available', info))
  autoUpdater.on('update-downloaded', info => event.sender.send('autoUpdateNotification', 'update-downloaded', info))
  autoUpdater.on('update-not-available', info => event.sender.send('autoUpdateNotification', 'update-not-available', info))
  autoUpdater.on('checking-for-update', () => event.sender.send('autoUpdateNotification', 'checking-for-update'))
  autoUpdater.on('error', err => event.sender.send('autoUpdateNotification', 'realerror', err))
}

// Open channel to listen for update actions.
ipcMain.on('autoUpdateAction', (event, arg, data) => {
  switch (arg) {
    case 'initAutoUpdater':
      console.log('Initializing auto updater.')
      initAutoUpdater(event, data)
      event.sender.send('autoUpdateNotification', 'ready')
      break

    case 'checkForUpdate':
      autoUpdater.checkForUpdates()
        .catch(err => {
          captureException(err)
          event.sender.send('autoUpdateNotification', 'realerror', err)
        })
      break

    case 'allowPrereleaseChange':
      if (!data) {
        const preRelComp = prerelease(app.getVersion())
        if (preRelComp != null && preRelComp.length > 0) autoUpdater.allowPrerelease = true
        else autoUpdater.allowPrerelease = data
      } else {
        autoUpdater.allowPrerelease = data
      }
      break

    case 'installUpdateNow':
      autoUpdater.quitAndInstall()
      break

    default:
      console.log('Unknown argument', arg)
      break
  }
})

// Redirect distribution index event from preloader to renderer.
ipcMain.on('distributionIndexDone', (event, res) => {
  event.sender.send('distributionIndexDone', res)
})

// Disable hardware acceleration.
// https://electronjs.org/docs/tutorial/offscreen-rendering
app.disableHardwareAcceleration()

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

function createWindow() {
  win = new BrowserWindow({
    width: 980,
    minWidth: 980,
    height: 552,
    minHeight: 552,
    title: 'Launcher SparksMC',
    icon: getPlatformIcon('icon'),
    frame: false,
    backgroundColor: '#171614',
    webPreferences: {
      preload: join(__dirname, 'app', 'assets', 'js', 'preloader.js'),
      nodeIntegration: true
    }
  })

  win.loadURL(format({
    pathname: join(__dirname, 'app', 'app.ejs'),
    protocol: 'file:',
    slashes: true
  }))

  win.removeMenu()
  win.resizable = true

  win.on('closed', () => {
    win = null
  })
}

function createMenu() {
  if (process.platform === 'darwin') {
    // Extend default included application menu to continue support for quit keyboard shortcut
    const applicationSubMenu = {
      label: 'Application',
      submenu: [{
        label: 'Quitter',
        accelerator: 'Command+Q',
        click: () => app.quit()
      }]
    }

    // Bundle submenus into a single template and build a menu object with it
    const menuObject = Menu.buildFromTemplate(applicationSubMenu)

    // Assign it to the application
    Menu.setApplicationMenu(menuObject)
  }
}

function getPlatformIcon(filename) {
  const opSys = process.platform

  if (opSys === 'darwin') {
    filename = filename + '.icns'
  } else if (opSys === 'win32') {
    filename = filename + '.ico'
  } else {
    filename = filename + '.png'
  }

  return join(__dirname, 'app', 'assets', 'images', filename)
}

app.on('ready', createWindow)
app.on('ready', createMenu)

app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})
